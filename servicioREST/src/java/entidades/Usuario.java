/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

/**
 *
 * @author jorge
 */
public class Usuario {
    private int id = 0;
    private String nombre = new String();
    private String email = new String();
    private String direccion = new String();
    private int edad = 0;
    private int semestre = 0;
    private String carrera = new String();

    public Usuario() {
    }
    
    public Usuario(int id, String nombre,String email,String direccion,int edad, int semestre, String carrera){
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.direccion = direccion;
        this.edad = edad;
        this.semestre = semestre;
        this.carrera = carrera;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }
    
    
    
    
    
}
