/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import com.google.gson.Gson;
import entidades.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jorge
 */
public class servicioWeb extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            //Generar mi json
            Gson gson = new Gson();
            List<Usuario> usuarios = new ArrayList();
            
            Usuario user = new Usuario();
            user.setId(16329);
            user.setNombre("Jorge Alberto Norzagaray Mora");
            user.setDireccion("Conocida");
            user.setEmail("jorge.norzagaray16329@potros.itson.edu.mx");
            user.setCarrera("Ingenieria en software");
            user.setSemestre(12);
            user.setEdad(31);
            
            usuarios.add(user);
            
            Usuario user2 = new Usuario();
            user2.setId(140651);
            user2.setNombre("Andres Roberto Marquez Naranjo");
            user2.setDireccion("Conocida");
            user2.setEmail("andres.marquez140651@potros.itson.edu.mx");
            user2.setCarrera("Ingenieria en software");
            user2.setSemestre(5);
            user2.setEdad(26);
            
            usuarios.add(user2);
            
            
            Usuario user3 = new Usuario();
            user3.setId(216695);
            user3.setNombre("Melissa Abigail Murillo Sanchez");
            user3.setDireccion("Conocida");
            user3.setEmail("melissa.murillo216695@potros.itson.edu.mx");
            user3.setCarrera("Ingenieria en software");
            user3.setSemestre(5);
            user3.setEdad(19);
            
            usuarios.add(user3);
            
            String _json = gson.toJson(usuarios);
            
            out.println(_json);
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
