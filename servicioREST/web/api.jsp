<%-- 
    Document   : api
    Created on : 04-nov-2020, 16:37:02
    Author     : jorge
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="entidades.Usuario"%>
<%@page import="com.google.gson.Gson"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
        
<%

     //Generar mi json
            Gson gson = new Gson();
            List<Usuario> usuarios = new ArrayList();
            
            Usuario user = new Usuario();
            user.setId(16329);
            user.setNombre("Jorge Alberto Norzagaray Mora");
            user.setDireccion("Conocida");
            user.setEmail("jorge.norzagaray16329@potros.itson.edu.mx");
            user.setCarrera("Ingenieria en software");
            user.setSemestre(12);
            user.setEdad(31);
            
            usuarios.add(user);
            
            Usuario user2 = new Usuario();
            user2.setId(140651);
            user2.setNombre("Andres Roberto Marquez Naranjo");
            user2.setDireccion("Conocida");
            user2.setEmail("andres.marquez140651@potros.itson.edu.mx");
            user2.setCarrera("Ingenieria en software");
            user2.setSemestre(5);
            user2.setEdad(26);
            
            usuarios.add(user2);
            
            
            Usuario user3 = new Usuario();
            user3.setId(216695);
            user3.setNombre("Melissa Abigail Murillo Sanchez");
            user3.setDireccion("Conocida");
            user3.setEmail("melissa.murillo216695@potros.itson.edu.mx");
            user3.setCarrera("Ingenieria en software");
            user3.setSemestre(5);
            user3.setEdad(19);
            
            usuarios.add(user3);
            
            String _json = gson.toJson(usuarios);
            
            out.println(_json);

%>