/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leerxml;

import entidades.channel;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author jorgenorza
 */
public class LeerXML {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        try {
            
            File file = new File("/home/jorgenorza/Desktop/channel.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(channel.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            channel canal = (channel) jaxbUnmarshaller.unmarshal(file);
            
//            System.out.println("canal title: "+canal.getTitle());

            System.out.println("\t====== Channel ========");
            System.out.println("\tTitle: "+canal.getTitle());
            System.out.println("\tLink: "+canal.getLink());
            System.out.println("\tDescrption: "+canal.getDescription());
            
            System.out.println("\t====== Item ========");
            System.out.println("\tTitle: "+canal.getItem().getTitle());
            System.out.println("\tLink: "+canal.getItem().getLink());
            System.out.println("\tDescrption: "+canal.getItem().getDescription());
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        
    }
    
}
