/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/**
 *
 * @author jorgenorza
 */
@XmlRootElement(name = "channel")
@XmlAccessorType(XmlAccessType.FIELD)
public class channel {
    
    @XmlElement(name = "title")
    private String title = new String();
    
    @XmlElement(name = "link")
    private String link = new String();
    
    @XmlElement(name = "description")
    private String description = new String();
    
    @XmlElement(name = "item")
    private item item = new item();

    public channel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public item getItem() {
        return item;
    }

    public void setItem(item item) {
        this.item = item;
    }
    
    
    
    
}
