/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsonapp_02;

import com.google.gson.Gson;
import entidades.tipoCambio;
import util.archivo;

/**
 *
 * @author jorgenorza
 */
public class JsonApp_02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        tipoCambio tc = new tipoCambio();
        
        Gson gson = new Gson();
        
        
        //Lectura del archivo
        String nombreArchivo = "/home/jorgenorza/Downloads/tipoCambio.json";
        archivo _archivo = new archivo(nombreArchivo);
        if(_archivo.abrir()){

            _archivo.leer();

            tc = gson.fromJson(_archivo.getTexto().toString(), tipoCambio.class);
            
            
            
        }else{
            System.out.println("No se pudo abrir el archivo...");
        }

        _archivo.cerrar();
        
        
        //Uso de los datos
        System.out.println("Titulo: "+tc.getTitulo());
        System.out.println("Serie: "+tc.getSerie());
        System.out.println("Decimales: "+tc.getDecimales());
        System.out.println("Unidades: "+tc.getUnidades());
        System.out.println("Grupo de Unidades: "+tc.getGrupoUnidades());
        System.out.println("Grupo de Unidades Orden: "+tc.getGrupoUnidadesOrden());
        System.out.println("==========VALORES==================");
        for(int i=0; i<tc.getValores().length; i++){
//            for(int j=0; j<tc.getValores()[i].length; j++){
//                
//            }
//               0 - Fecha
               // 1- tipoCambio
               if(tc.getValores()[i][0].contains("2020-09") || tc.getValores()[i][0].contains("2020-10") )
                    System.out.println("Fecha: "+tc.getValores()[i][0]+" - tipo de Cambio: "+tc.getValores()[i][1]);
               
        }
        
        
    }
    
}
