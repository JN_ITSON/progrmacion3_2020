/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jorgenorza
 */
public class tipoCambio {
    private String titulo = new String();
    private String serie = new String();
//    private List<valores> valores = new ArrayList();
    private String valores[][];
    private int decimales = 0;
    private String unidades = new String();
    private int grupoUnidades = 0;
    private int grupoUnidadesOrden = 0;

    public tipoCambio() {
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

//    public List<valores> getValores() {
//        return valores;
//    }
//
//    public void setValores(List<valores> valores) {
//        this.valores = valores;
//    }

    public int getDecimales() {
        return decimales;
    }

    public void setDecimales(int decimales) {
        this.decimales = decimales;
    }

    public String getUnidades() {
        return unidades;
    }

    public void setUnidades(String unidades) {
        this.unidades = unidades;
    }

    public int getGrupoUnidades() {
        return grupoUnidades;
    }

    public void setGrupoUnidades(int grupoUnidades) {
        this.grupoUnidades = grupoUnidades;
    }

    public int getGrupoUnidadesOrden() {
        return grupoUnidadesOrden;
    }

    public void setGrupoUnidadesOrden(int grupoUnidadesOrden) {
        this.grupoUnidadesOrden = grupoUnidadesOrden;
    }

    public String[][] getValores() {
        return valores;
    }

    public void setValores(String[][] valores) {
        this.valores = valores;
    }
    
    
    
}
