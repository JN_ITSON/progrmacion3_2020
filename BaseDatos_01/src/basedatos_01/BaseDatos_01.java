
package basedatos_01;

import configuracion.DBHelper;
import entidades.User;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jorge
 */
public class BaseDatos_01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Configuración
        StringBuilder sql = new StringBuilder();
        List<User> usuarios = new ArrayList();   
        DBHelper db = new DBHelper();
        
        //Consulta SQL        
        sql.append("SELECT * FROM user ;");
        
        
        //Conexion a la base de datos
        try{
            
            if(db.connect()){ //Abre la conexion a la base de datos
                //Hacemos consultas
                
              ResultSet rs =(ResultSet)  db.execute(sql.toString(), false);
              
              while(rs.next()){
                  User userTmp = new User();
                  
                  userTmp.setId(rs.getInt("id"));
                  userTmp.setNombre(rs.getString("nombre"));
                  userTmp.setEmail(rs.getString("email"));
                  userTmp.setContrasena(rs.getString("contrasena"));
                  
                  usuarios.add(userTmp);
//                  
//                  
//                  System.out.println("Nombre: "+ rs.getString(2));
//                  System.out.println("Nombre: "+ rs.getString("nombre"));
//                  System.out.println("====================================");
              }
              
              
              for(User user : usuarios){
                  System.out.println("ID: \t\t"+ user.getId());
                  System.out.println("Nombre: \t\t"+ user.getNombre());
                  System.out.println("Email: \t\t"+ user.getEmail());
                  System.out.println("Contraseña: \t\t"+ user.getContrasena());
                  System.out.println("========================================");
              }
              
              
                
            }else{
                System.out.println("Error de conexion: "+db.getMensajeError());
            }
            
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            db.disconnect();
        }
        
        
        /*
        
        ASIGNACIÓN
        Consultar como crear las siguientes consultas 
        --------------
        ----user------
        --------------
        int id
        varchar nombre
        varchar email
        varchar contrasena        
        
        CREATE (INSERT INTO)<--
        UPDATE (UPDATE)<--
        DELETE (DELETE)<--
        CONSULTAR TODO (SELECT)<-- SELECT * FROM user
        CONSULTAR POR ID (SELECT + WHERE)<--
        
        
        
        
        */
        
        
    }
    
}
