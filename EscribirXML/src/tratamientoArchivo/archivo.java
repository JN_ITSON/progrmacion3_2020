package tratamientoArchivo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author juan_m_osuna
 */
public final class archivo {
    
    private FileReader archivoLeer;
    private BufferedReader bufferReader;
    
    private FileWriter archivoEscribir;
    private PrintWriter impresoraEscribir;
    
    private String nombreArchivo = new String();
    private StringBuilder texto = new StringBuilder();

    //Instancia
    public archivo(String nombreArchivo) {
        setNombreArchivo(nombreArchivo);
    }
    
    //Metodos
    public boolean crear(){
        
        try{
            
            setArchivoEscribir(new FileWriter(getNombreArchivo()));
            setImpresoraEscribir(new PrintWriter(getArchivoEscribir()));
            
        }catch(IOException ex){
            
            System.out.println("No se pudo crear el archivo ...");
            return false;
        }
        
        return true;
    }
    
    public boolean abrir(){
        
        try{
            
            setArchivo(new FileReader(getNombreArchivo()));
            setBufferReader(new BufferedReader(getArchivoLeer()));
            
        }catch(FileNotFoundException ex){
            
            System.out.println("Archivo no encontrado ...");
            return false;
        }
        
        return true;
        
    }
    
    public void leer(){
        
        String linea = new String();
        
        try{
        
            while((linea = getBufferReader().readLine()) != null) {
                setTexto(linea);
            }
            
        }catch(IOException ex){
            
            System.out.println("No se pudo leer el archivo ...");
            
        }
    }
    
    public void escribir(){
        getImpresoraEscribir().println(getTexto());
    }
    
    public void cerrar(){
        
        try{
        
            if (getArchivoLeer() != null){
                if (getBufferReader() != null){
                    getBufferReader().close();
                }
                getArchivoLeer().close();
            }

            if (this.getArchivoEscribir() != null){
                if (getImpresoraEscribir() != null){
                    getImpresoraEscribir().close();
                }
                getArchivoEscribir().close();
            }
        
        }catch(IOException ex){
            
            System.out.println("No se pudo cerrar el archivo ...");
            
        }
        
    }
    
    //Atributos
    public FileReader getArchivo() {
        return archivoLeer;
    }

    public void setArchivo(FileReader archivo) {
        this.archivoLeer = archivo;
    }

    public BufferedReader getBufferReader() {
        return bufferReader;
    }

    public void setBufferReader(BufferedReader bufferReader) {
        this.bufferReader = bufferReader;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public StringBuilder getTexto() {
        return texto;
    }

    public void setTexto(String linea) {
        this.texto.append(linea+"\n");
    }

    public FileWriter getArchivoEscribir() {
        return archivoEscribir;
    }

    public void setArchivoEscribir(FileWriter archivoEscribir) {
        this.archivoEscribir = archivoEscribir;
    }

    public PrintWriter getImpresoraEscribir() {
        return impresoraEscribir;
    }

    public void setImpresoraEscribir(PrintWriter impresoraEscribir) {
        this.impresoraEscribir = impresoraEscribir;
    }

    public FileReader getArchivoLeer() {
        return archivoLeer;
    }

    public void setArchivoLeer(FileReader archivoLeer) {
        this.archivoLeer = archivoLeer;
    }
    
}
