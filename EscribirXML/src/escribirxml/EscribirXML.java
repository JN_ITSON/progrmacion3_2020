/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package escribirxml;

import entidades.Producto2;
import entidades.Producto3;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import tratamientoArchivo.archivo;

/**
 *
 * @author jorge
 */
public class EscribirXML {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws JAXBException {
       //Construir mi objeto producto
       
       Producto2 producto = new Producto2();
       producto.setId(100);
       producto.setNombre("Desodorante en barra");
       producto.setDescripcion("Desodorante en barra con aroma dulces noches");
       producto.setMarca("Axe");
       producto.setCantidad(15);
       producto.setPrecioUnitario(40);
       producto.setPrecio(15*40);
       
       //Generar el xml
       JAXBContext jaxbContext = JAXBContext.newInstance(Producto2.class);
       // Unmarshaller  -> Lectura
       Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
       jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
       StringWriter productoWriter = new StringWriter();
       jaxbMarshaller.marshal(producto, productoWriter);
       
        //System.out.println(productoWriter.toString());
        
        //Prueba 1
//        String ruta_1 = "E://producto.xml";
        
        
        //Prueba 2 (Todos sean Nodos)
        String ruta_2 = "E://producto2_1.xml";
        
        //Prueba 3 (Mixto de atributos y nodos)
//        String ruta_3 = "E://producto3.xml";
        
        archivo _archivo = new archivo(ruta_2);
        
        if(_archivo.crear()){
            _archivo.setTexto(productoWriter.toString());
            _archivo.escribir();
        }
        
        _archivo.cerrar();
    }
    
}
