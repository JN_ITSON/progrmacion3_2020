
package jsonapp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import entidades.Materia;
import entidades.User;
import entidades.Users;
import java.util.ArrayList;
import java.util.List;
import util.archivo;

public class JsonApp {

    public static void main(String[] args) {
        
        List<User> usuarios = new ArrayList();
        boolean crearArchivo = false;
        
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        
        //Agregar 2 usuarios 
        List<Materia> mat1 = new ArrayList();
        mat1.add(new Materia(123,"Programacion III ",5));
        mat1.add(new Materia(124,"Arquitectura de Software ",3));
        mat1.add(new Materia(125,"Optativa IV ",7));
        User u1 = new User(1,"Diego","diego@gmail.com",mat1);
        usuarios.add(new User(1,"Diego","diego@gmail.com",mat1));
        
        
        List<Materia> mat2 = new ArrayList();
        mat2.add(new Materia(123,"Vida Saludable",1));
        mat2.add(new Materia(124,"Topicos de Aplicaciones",7));
        mat2.add(new Materia(125,"Redes",3));
        usuarios.add(new User(2,"Nohemi","nohemi@gmail.com",mat2));
       
        String json = gson.toJson(usuarios); 
//       String json = "";
        
        //System.out.println(json);
       String nombreArchivo = "usuario.json";
        archivo _archivo = new archivo(nombreArchivo);
        
        /* Crear archivo JSON */
        if(crearArchivo){
            if(_archivo.crear()){
                _archivo.setTexto(json);
                _archivo.escribir();
            }else{
                System.out.println("No se pudo crear el archivo..");
            }

            _archivo.cerrar();
        }else{
       
            if(_archivo.abrir()){
                
                _archivo.leer();
                
//                User u2 = gson.fromJson(_archivo.getTexto().toString(), User.class);
//                System.out.println("Usuario1 nombre: "+u2.getNombre());
//                for(Materia m : u2.getMaterias()){
//                    System.out.println("Materia "+m.getNombre());
//                }

                User[]  usuariosFromJson =  gson.fromJson(_archivo.getTexto().toString(), User[].class);
                for(User u: usuariosFromJson){
                    System.out.println("Nombre "+u.getNombre());
                }
            }else{
                System.out.println("No se pudo abrir el archivo...");
            }

            _archivo.cerrar();
        }
        
        
    }
    
}
