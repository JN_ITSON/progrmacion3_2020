
package entidades;

import java.util.ArrayList;
import java.util.List;

public class User {
    private int id = 0;
    private String nombre = new String();
    private String email = new String();
    private List<Materia> materias = new ArrayList();

    public User() {
    }
    
    public User(int id, String nombre, String email, List<Materia> materias){
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.materias = materias;
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Materia> getMaterias() {
        return materias;
    }

    public void setMaterias(List<Materia> materias) {
        this.materias = materias;
    }
    
    
    
    
}
