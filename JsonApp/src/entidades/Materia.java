
package entidades;

public class Materia {
    private int codigo = 0;
    private String nombre = new String();
    private int semestre = 0;

    public Materia() {
    }
    
    public Materia(int codigo, String nombre, int semestre) {
    
            this.codigo = codigo;
            this.nombre = nombre;
            this.semestre = semestre;
    
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }
    
    
    
}
